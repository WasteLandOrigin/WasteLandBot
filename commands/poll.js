const { SlashCommandBuilder } = require("@discordjs/builders");
var color = require("cli-color");

module.exports = {
  data: new SlashCommandBuilder()
      .setName("poll")
      .setDescription("Create a poll")
      .addStringOption((option) =>
          option
          .setName("polltheme")
          .setDescription("Poll theme")
          .setRequired(true)
      )
      .addStringOption((option) =>
          option
          .setName("var1")
          .setDescription("Variant")
          .setRequired(true)
      )
      .addStringOption((option) =>
          option
          .setName("emoji1")
          .setDescription("Select emoji")
          .setRequired(true)
      )
      .addStringOption((option) =>
          option
          .setName("var2")
          .setDescription("Variant")
          .setRequired(true)
      )
      .addStringOption((option) =>
          option
          .setName("emoji2")
          .setDescription("Select emoji")
          .setRequired(true)
      )
      .addStringOption((option) =>
          option
          .setName("var3")
          .setDescription("Variant")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("emoji3")
          .setDescription("Select emoji")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("var4")
          .setDescription("Variant")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("emoji4")
          .setDescription("Select emoji")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("var5")
          .setDescription("Variant")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("emoji5")
          .setDescription("Select emoji")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("var6")
          .setDescription("Variant")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("emoji6")
          .setDescription("Select emoji")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("var7")
          .setDescription("Variant")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("emoji7")
          .setDescription("Select emoji")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("var8")
          .setDescription("Variant")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("emoji8")
          .setDescription("Select emoji")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("var9")
          .setDescription("Variant")
          .setRequired(false)
      )
      .addStringOption((option) =>
          option
          .setName("emoji9")
          .setDescription("Select emoji")
          .setRequired(false)
      ),

  async execute(interaction) {
      const { options, Infraction } = interaction;

      let var1 = " : " + options.getString("var1");
      let var2 = " : " + options.getString("var2");
      let var3 = options.getString("var3");
      let var4 = options.getString("var4");
      let var5 = options.getString("var5");
      let var6 = options.getString("var6");
      let var7 = options.getString("var7");
      let var8 = options.getString("var8");
      let var9 = options.getString("var9");
      let emoji1 = options.getString("emoji1");
      let emoji2 = options.getString("emoji2");
      let emoji3 = options.getString("emoji3");
      let emoji4 = options.getString("emoji4");
      let emoji5 = options.getString("emoji5");
      let emoji6 = options.getString("emoji6");
      let emoji7 = options.getString("emoji7");
      let emoji8 = options.getString("emoji8");
      let emoji9 = options.getString("emoji9");
      const polltheme = options.getString("polltheme")

      em3 = true;
      em4 = true;
      em5 = true;
      em6 = true;
      em7 = true;
      em8 = true;
      em9 = true;

      if (var3 === null) {
          var3 = "";
      } else {
          var3 = " : " + var3;
      }
      if (emoji3 === null) {
          emoji3 = "";
          em3 = false;
      }
      if (var4 === null) {
          var4 = "";
      } else {
          var4 = " : " + var4;
      }
      if (emoji4 === null) {
          emoji4 = "";
          em4 = false;
      }
      if (var5 === null) {
          var5 = "";
      } else {
          var5 = " : " + var5;
      }
      if (emoji5 === null) {
          emoji5 = "";
          em5 = false;
      }
      if (var6 === null) {
          var6 = "";
      } else {
          var6 = " : " + var6;
      }
      if (emoji6 === null) {
          emoji6 = "";
          em6 = false;
      }
      if (var7 === null) {
          var7 = "";
      } else {
          var7 = " : " + var7;
      }
      if (emoji7 === null) {
          emoji7 = "";
          em7 = false;
      }
      if (var8 === null) {
          var8 = "";
      } else {
          var8 = " : " + var8;
      }
      if (emoji8 === null) {
          emoji8 = "";
          em8 = false;
      }
      if (var9 === null) {
          var9 = "";
      } else {
          var9 = " : " + var9;
      }
      if (emoji9 === null) {
          emoji9 = "";
          em9 = false;
      }

      await interaction.reply({
          content: `### ${polltheme} \n${emoji1} ${var1} \n${emoji2} ${var2} \n${emoji3} ${var3} \n${emoji4} ${var4} \n${emoji5} ${var5} \n${emoji6} ${var6} \n${emoji7} ${var7} \n${emoji8} ${var8} \n${emoji9} ${var9}`,
          emphemeral: true,
          fetchReply:true
      }).then((msg) => {
          try {
              msg.react(`${emoji1}`);
              msg.react(`${emoji2}`);
              if (em3 === true){
                  msg.react(`${emoji3}`);
              }
              if (em4 === true){
                  msg.react(`${emoji4}`);
              }
              if (em5 === true){
                  msg.react(`${emoji5}`);
              }
              if (em6 === true){
                  msg.react(`${emoji6}`);
              }
              if (em7 === true){
                  msg.react(`${emoji7}`);
              }
              if (em8 === true){
                  msg.react(`${emoji8}`);
              }
              if (em9 === true){
                  msg.react(`${emoji9}`);
              }
          } catch (error) {
              console.error(color.red("Poll error!" + error));
          }
      });
      console.log(color.cyanBright("Poll created!"));
  }
}
