const { SlashCommandBuilder } = require("@discordjs/builders");
var color = require("cli-color");

module.exports = {
    data: new SlashCommandBuilder()
        .setName("info")
        .setDescription("Information about this bot"),
    async execute(interaction) {
        interaction.reply({
            content: "**Developer**: SpiritOTHawk \nDeveloped for WasteLand Origin community \nHow to contact with us? Use <#1019658058034581514> channel or write to email or write to matrix \n**Official support email**: spiritofthehawk@proton.me \n**Official Matrix contact**: @SpiritOTHawk:tchncs.de  \n**Official Mastodon page**: https://mastodon.social/@wlorigin \nBot license: **AGPL-3.0-or-later** \nSource code: https://codeberg.org/WasteLandOrigin/WasteLandBot \nHow i can connect to minecraft server? See #connect channel \nCan i join your minecraft server? Yeah! Just fill out the form https://forms.gle/96c1pbPEfi8Z3yJ98 and write to @spiritofthehawk",
            emphemeral: true
        });
        console.log(color.cyanBright("Command info executed"));
    }
};
