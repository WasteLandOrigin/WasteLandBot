![cute](https://forthebadge.com/images/featured/featured-contains-cat-gifs.svg)

# WasteLandBot
### Discord bot for WasteLand.
![0001](https://user-images.githubusercontent.com/76490476/165584096-9c8d4787-5ca1-4503-9533-cc67f3bd40e8.jpg)

## Info

We have completely switched to slash commands.

![Безымянный11](https://user-images.githubusercontent.com/76490476/165584861-e1f85080-2d9e-48d3-8a47-e6ed8a443959.jpg)

To invite this bot to your server, run the /invite command in our discord ( https://discord.gg/UBaauaN )

![Безымянный1](https://user-images.githubusercontent.com/76490476/165584922-305b6c7e-beb0-4184-bb39-4e0ef9437546.jpg)

By default, the commands do not work on your server, to activate them write @spiritofthehawk

## Work on:
![Linux](https://img.shields.io/badge/Linux-FCC624?logo=linux&logoColor=black)
![Debian](https://img.shields.io/badge/Debian-A81D33?logo=debian&logoColor=fff)
![Artix Linux](https://img.shields.io/badge/Artix%20Linux-10A0CC?logo=artixlinux&logoColor=fff)
![Void Linux](https://img.shields.io/badge/Void%20Linux-478061?logo=voidlinux&logoColor=fff)
![Android](https://img.shields.io/badge/Android-3DDC84?logo=android&logoColor=white)

## Uses:
![pnpm](https://img.shields.io/badge/pnpm-F69220?logo=pnpm&logoColor=fff)
[![Discord](https://img.shields.io/badge/Discord-%235865F2.svg?&logo=discord&logoColor=white)](https://discord.gg/UBaauaN)
![NodeJS](https://img.shields.io/badge/Node.js-6DA55F?logo=node.js&logoColor=white)

## License
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL_v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## Social
<p align="center">
    <a href="https://discord.gg/UBaauaN">
        <img src="https://img.shields.io/badge/Discord-%235865F2.svg?&logo=discord&logoColor=white">
            </a>
    <a href="https://mastodon.social/@wlorigin">
        <img src="https://img.shields.io/mastodon/follow/112151761663236004">
            </a>
    <a href="https://mastodon.social/@SpiritOTHawk">
        <img src="https://img.shields.io/mastodon/follow/110688157603004224">
            </a>
</p>